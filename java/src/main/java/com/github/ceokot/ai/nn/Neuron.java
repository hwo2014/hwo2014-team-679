package com.github.ceokot.ai.nn;

import com.github.ceokot.kotraider.util.Utils;

/**
 * @author ceokot
 */
public class Neuron {
    public int numInputs;
    public double[] weights;

    public Neuron(int numInputs) {
        this.numInputs = numInputs + 1;
        weights = new double[this.numInputs];

        for (int i = 0; i < weights.length; ++i) {
            weights[i] = Utils.randomClamped();
        }
    }
}