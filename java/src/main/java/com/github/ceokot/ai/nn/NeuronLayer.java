package com.github.ceokot.ai.nn;

/**
 * @author ceokot
 */
public class NeuronLayer {
    public final int numNeurons;
    public Neuron[] neurons;

    public NeuronLayer(int numNeurons, int numInputsPerNeuron) {
        this.numNeurons = numNeurons;
        neurons = new Neuron[numNeurons];

        for (int i = 0; i < numNeurons; ++i) {
            neurons[i] = new Neuron(numInputsPerNeuron);
        }
    }
}