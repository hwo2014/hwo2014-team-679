package com.github.ceokot.ai.nn;

import com.github.ceokot.kotraider.util.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ceokot
 */
public class NeuralNetwork {
    private final int numInputs;
    private final int numOutputs;
    private final int numHiddenLayers;
    private final int neuronsPerHiddenLayer;

    private List<NeuronLayer> neuronLayers = new ArrayList<>();

    public NeuralNetwork() {
        numInputs = Constants.NUMBER_INPUTS;
        numOutputs = Constants.NUMBER_OUTPUTS;
        numHiddenLayers = Constants.NUMBER_HIDDEN_LAYERS;
        neuronsPerHiddenLayer = Constants.NEURONS_PER_HIDDEN_LAYER;

        createNet();
    }

    public void createNet() {
        if (numHiddenLayers > 0) {
            //create first hidden layer
            neuronLayers.add(new NeuronLayer(neuronsPerHiddenLayer, numInputs));

            for (int i = 0; i < numHiddenLayers - 1; ++i) {
                neuronLayers.add(new NeuronLayer(neuronsPerHiddenLayer, neuronsPerHiddenLayer));
            }

            //create output layer
            neuronLayers.add(new NeuronLayer(numOutputs, neuronsPerHiddenLayer));
        } else {
            //create output layer
            neuronLayers.add(new NeuronLayer(numOutputs, numInputs));
        }
    }

    public List<Double> getWeights() {
        List<Double> weights = new ArrayList<>();

        //for each layer
        for (int i = 0; i < numHiddenLayers + 1; ++i) {
            //for each neuron
            for (int j = 0; j < neuronLayers.get(i).numNeurons; ++j) {
                //for each weight
                for (int k = 0; k < neuronLayers.get(i).neurons[j].numInputs; ++k) {
                    weights.add(neuronLayers.get(i).neurons[j].weights[k]);
                }
            }
        }

        return weights;
    }

    public int getNumberOfWeights() {
        int weights = 0;

        //for each layer
        for (int i = 0; i < numHiddenLayers + 1; ++i) {
            //for each neuron
            for (int j = 0; j < neuronLayers.get(i).numNeurons; ++j) {
                //for each weight
                for (int k = 0; k < neuronLayers.get(i).neurons[j].numInputs; ++k) {
                    weights++;
                }
            }
        }

        return weights;
    }

    public void putWeights(List<Double> weights) {
        int cWeight = 0;

        //for each layer
        for (int i = 0; i < numHiddenLayers + 1; ++i) {
            //for each neuron
            for (int j = 0; j < neuronLayers.get(i).numNeurons; ++j) {
                //for each weight
                for (int k = 0; k < neuronLayers.get(i).neurons[j].numInputs; ++k) {
                    neuronLayers.get(i).neurons[j].weights[k] = weights.get(cWeight++);
                }
            }
        }
    }

    public List<Double> update(List<Double> inputs) {
        List<Double> outputs = new ArrayList<>();
        int cWeight;

        if (inputs.size() != numInputs) {
            return outputs;
        }

        //For each layer....
        for (int i = 0; i < numHiddenLayers + 1; ++i) {
            if (i > 0) {
                inputs.clear();
                for (Double output : outputs) {
                    inputs.add(output);
                }
            }

            outputs.clear();

            cWeight = 0;

            for (int j = 0; j < neuronLayers.get(i).numNeurons; ++j) {
                double netInput = 0;
                int numInputs = neuronLayers.get(i).neurons[j].numInputs;

                for (int k = 0; k < numInputs - 1; ++k) {
                    netInput += neuronLayers.get(i).neurons[j].weights[k] * inputs.get(cWeight++);
                }

                netInput += neuronLayers.get(i).neurons[j].weights[numInputs - 1] * Constants.BIAS;
                outputs.add(sigmoid(netInput, Constants.ACTIVATION_RESPONSE));
                cWeight = 0;
            }
        }

        return outputs;
    }

    public double sigmoid(double netInput, double response) {
        return (1 / (1 + Math.exp(-netInput / response)));
    }
}
