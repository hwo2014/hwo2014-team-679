package com.github.ceokot.kotraider.message.detail;

import com.google.gson.annotations.SerializedName;

public class TrackPiece {
    @SerializedName("switch")
    public final boolean switchLane;
    public final boolean bridge;
    public final double radius;
    public final double angle;
    public final double length;

    public TrackPiece(boolean switchLane, boolean bridge, double length, double radius, double angle) {
        this.switchLane = switchLane;
        this.bridge = bridge;
        this.radius = radius;
        this.angle = angle;
        this.length = length;
    }

    public boolean isBend(){
        return radius > 0;
    }
}
