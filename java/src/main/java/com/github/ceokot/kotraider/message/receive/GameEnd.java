package com.github.ceokot.kotraider.message.receive;

import com.github.ceokot.kotraider.message.detail.CarLapResult;
import com.github.ceokot.kotraider.message.BotMsg;

import java.util.List;

public class GameEnd extends BotMsg {
    public final List<CarLapResult> results;
    public final List<CarLapResult> bestLaps;

    public GameEnd(int gameTick, List<CarLapResult> results, List<CarLapResult> bestLaps) {
        super(gameTick);
        this.results = results;
        this.bestLaps = bestLaps;
    }

    @Override
    protected String msgType() {
        return "gameEnd";
    }

}
