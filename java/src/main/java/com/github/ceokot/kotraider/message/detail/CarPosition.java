package com.github.ceokot.kotraider.message.detail;

public class CarPosition {
    public final CarId id;
    public final double angle;
    public final PiecePosition piecePosition;

    public CarPosition(CarId id, double angle, PiecePosition piecePosition) {
        this.id = id;
        this.angle = angle;
        this.piecePosition = piecePosition;
    }
}
