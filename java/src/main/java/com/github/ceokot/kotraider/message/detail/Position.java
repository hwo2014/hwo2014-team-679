package com.github.ceokot.kotraider.message.detail;

public class Position {
    public final double x;
    public final double y;

    public Position(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
