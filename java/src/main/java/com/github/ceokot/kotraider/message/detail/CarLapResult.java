package com.github.ceokot.kotraider.message.detail;

public class CarLapResult {
    public final CarId car;
    public final LapResult result;

    public CarLapResult(CarId car, LapResult result) {
        this.car = car;
        this.result = result;
    }
}
