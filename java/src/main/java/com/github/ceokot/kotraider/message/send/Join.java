package com.github.ceokot.kotraider.message.send;

import com.github.ceokot.kotraider.message.BotMsg;

public class Join extends BotMsg {
    public final String name;
    public final String key;

    public Join(int gameTick, final String name, final String key) {
        super(gameTick);
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }

}
