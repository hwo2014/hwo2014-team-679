package com.github.ceokot.kotraider.util;

public class Constants {
    public static final double dPi                 = 3.14159265358979;
    public static final double dHalfPi             = dPi / 2;
    public static final double dTwoPi              = dPi * 2;
    public static final int NUMBER_INPUTS = 6;
    public static final int NUMBER_HIDDEN_LAYERS = 1;
    public static final int NEURONS_PER_HIDDEN_LAYER = 6;
    public static final int NUMBER_OUTPUTS = 5;
    public static final double ACTIVATION_RESPONSE = 1;
    public static final double BIAS = -1;
    public static final double MAX_THROTTLE = 1.0;
    public static final double MIN_THROTTLE = 0.0;
    public static final double AVG_THROTTLE = 0.5;
    public static final String KEIMOLA_TRACK = "keimola";
    public static final String GERMANY_TRACK = "germany";
    public static final String USA_TRACK = "usa";
    public static final String FRANCE_TRACK = "france";
    public static final String ELAEINTARHA_TRACK = "elaeintarha";
    public static final String IMOLA_TRACK = "imola";
    public static final String ENGLAND_TRACK = "england";
    public static final String SUZUKA_TRACK = "suzuka";
}
