package com.github.ceokot.kotraider.message.receive;

import com.github.ceokot.kotraider.message.detail.Race;
import com.github.ceokot.kotraider.message.BotMsg;

public class GameInit extends BotMsg {
    public final Race race;

    public GameInit(int gameTick, Race race) {
        super(gameTick);
        this.race = race;
    }

    @Override
    protected String msgType() {
        return "gameInit";
    }

}
