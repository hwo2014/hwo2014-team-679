package com.github.ceokot.kotraider.message.detail;

public class Car {
    public final CarId id;
    public final Dimensions dimensions;

    public Car(CarId id, Dimensions dimensions) {
        this.id = id;
        this.dimensions = dimensions;
    }
}
