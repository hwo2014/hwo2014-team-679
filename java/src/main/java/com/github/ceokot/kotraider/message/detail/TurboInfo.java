package com.github.ceokot.kotraider.message.detail;

public class TurboInfo {
    public final double turboDurationMilliseconds;
    public final int turboDurationTicks;
    public final double turboFactor;

    public TurboInfo(double turboDurationMilliseconds, int turboDurationTicks, double turboFactor) {
        this.turboDurationMilliseconds = turboDurationMilliseconds;
        this.turboDurationTicks = turboDurationTicks;
        this.turboFactor = turboFactor;
    }
}
