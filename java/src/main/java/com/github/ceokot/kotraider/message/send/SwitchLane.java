package com.github.ceokot.kotraider.message.send;

import com.github.ceokot.kotraider.message.BotMsg;

public class SwitchLane extends BotMsg {
    private String direction;

    public SwitchLane(int gameTick, String direction) {
        super(gameTick);
        this.direction = direction;
    }

    @Override
    protected Object msgData() {
        return direction;
    }
    @Override
    protected String msgType() {
        return "switchLane";
    }

}
