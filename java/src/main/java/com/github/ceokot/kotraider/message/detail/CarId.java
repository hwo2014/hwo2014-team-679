package com.github.ceokot.kotraider.message.detail;

public class CarId {
    public final String name;
    public final String color;

    public CarId(String name, String color) {
        this.name = name;
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarId carId = (CarId) o;

        return color.equals(carId.color) && name.equals(carId.name);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + color.hashCode();
        return result;
    }
}
