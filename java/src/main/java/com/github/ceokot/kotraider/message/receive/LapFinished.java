package com.github.ceokot.kotraider.message.receive;

import com.github.ceokot.kotraider.message.detail.CarId;
import com.github.ceokot.kotraider.message.detail.LapResult;
import com.github.ceokot.kotraider.message.detail.Ranking;

public class LapFinished {
    public final CarId car;
    public final LapResult lapTime;
    public final LapResult raceTime;
    public final Ranking ranking;

    public LapFinished(int gameTick, CarId car, LapResult lapTime, LapResult raceTime, Ranking ranking) {
        this.car = car;
        this.lapTime = lapTime;
        this.raceTime = raceTime;
        this.ranking = ranking;
    }
}
