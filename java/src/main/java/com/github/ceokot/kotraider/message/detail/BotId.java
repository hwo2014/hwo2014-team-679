package com.github.ceokot.kotraider.message.detail;

public class BotId {
    public final String name;
    public final String key;

    public BotId(final String name, final String key) {
        this.name = name;
        this.key = key;
    }
}
