package com.github.ceokot.kotraider.message.send;

import com.github.ceokot.kotraider.message.BotMsg;

public class Throttle extends BotMsg {
    private double value;

    public Throttle(int gameTick, double value) {
        super(gameTick);
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }

}
