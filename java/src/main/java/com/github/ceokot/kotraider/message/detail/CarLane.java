package com.github.ceokot.kotraider.message.detail;

public class CarLane {
    public final int startLaneIndex;
    public final int endLaneIndex;

    public CarLane(int startLaneIndex, int endLaneIndex) {
        this.startLaneIndex = startLaneIndex;
        this.endLaneIndex = endLaneIndex;
    }
}
