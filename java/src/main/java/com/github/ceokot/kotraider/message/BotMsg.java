package com.github.ceokot.kotraider.message;

import com.google.gson.Gson;

public abstract class BotMsg {
    public final int gameTick;

    protected BotMsg(int gameTick) {
        this.gameTick = gameTick;
    }

    public String toJson() {
        return new Gson().toJson(new MsgWrapper(gameTick, this));
    }

    protected Object msgData() {
        return this;
    }
    protected abstract String msgType();

}

