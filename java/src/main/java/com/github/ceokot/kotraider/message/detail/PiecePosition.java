package com.github.ceokot.kotraider.message.detail;

public class PiecePosition {
    public final int pieceIndex;
    public final double inPieceDistance;
    public final CarLane lane;
    public final int lap;

    public PiecePosition(int pieceIndex, double inPieceDistance, CarLane lane, int lap) {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
        this.lane = lane;
        this.lap = lap;
    }
}
