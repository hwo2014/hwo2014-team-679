package com.github.ceokot.kotraider.message.detail;

public class Dimensions {
    public final double length;
    public final double width;
    public final double guideFlagPosition;
    public final double mass = 4.95;

    public Dimensions(double length, double width, double guideFlagPosition) {
        this.length = length;
        this.width = width;
        this.guideFlagPosition = guideFlagPosition;
    }
}
