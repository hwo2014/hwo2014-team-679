package com.github.ceokot.kotraider.util;

import java.util.Random;

/**
 * @author ceokot
 */
public class Utils {
    private static final Random randomGen = new Random();
    public Utils() {

    }

    /**
     * returns a random integer between x <= n <= y
     */
    public static int randInt(int x, int y) {
        return (int) (Math.random() * Math.abs(y - x + 1)) + x;
    }

    /**
     * @return a random float between 0 <= n < 1s
     */
    public static float randFloat() {
        return randomGen.nextFloat();
    }

    /**
     * @return a random boolean
     */
    public static boolean randBool() {
        return randInt(0, 1) == 1;
    }

    /**
     * @return a random float in the range -1 < n < 1
     */
    public static double randomClamped() {
        return randFloat() - randFloat();
    }

    //converts an integer to a String. Just an alias of Integer.toString
    public static String itos(int arg) {
        return Integer.toString(arg);
    }

    //converts an float to a String. Just an alias of Float.toString
    public static String ftos(float arg) {
        return Float.toString(arg);
    }

    /**
     * The arg value is modified if it is below the min threshold or above the max threshold
     */
    public static double clamp(double arg, double min, double max) {
        if (arg < min) {
            arg = min;
        }

        if (arg > max) {
            arg = max;
        }

        return arg;
    }
}
