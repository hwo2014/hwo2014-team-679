package com.github.ceokot.kotraider.message.send;

import com.github.ceokot.kotraider.message.BotMsg;

public class Ping extends BotMsg {
    public Ping(int gameTick) {
        super(gameTick);
    }

    @Override
    protected String msgType() {
        return "ping";
    }

}
