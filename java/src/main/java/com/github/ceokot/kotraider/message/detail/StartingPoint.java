package com.github.ceokot.kotraider.message.detail;

public class StartingPoint {
    public final Position position;
    public final double angle;

    public StartingPoint(Position position, double angle) {
        this.position = position;
        this.angle = angle;
    }
}
