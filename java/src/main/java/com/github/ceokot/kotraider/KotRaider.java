package com.github.ceokot.kotraider;

import com.github.ceokot.ai.nn.NeuralNetwork;
import com.github.ceokot.kotraider.message.BotMsg;
import com.github.ceokot.kotraider.message.detail.CarId;
import com.github.ceokot.kotraider.message.detail.CarPosition;
import com.github.ceokot.kotraider.message.detail.PiecePosition;
import com.github.ceokot.kotraider.message.detail.Race;
import com.github.ceokot.kotraider.message.detail.TrackPiece;
import com.github.ceokot.kotraider.message.detail.TurboInfo;
import com.github.ceokot.kotraider.message.receive.LapFinished;
import com.github.ceokot.kotraider.message.send.Ping;
import com.github.ceokot.kotraider.message.send.SwitchLane;
import com.github.ceokot.kotraider.message.send.Throttle;
import com.github.ceokot.kotraider.message.send.Turbo;
import com.github.ceokot.kotraider.util.Constants;
import com.github.ceokot.kotraider.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Charles.Okot
 */
public class KotRaider {

    NeuralNetwork neuralNetwork = new NeuralNetwork();

    double speed;
    double throttle;

    //to store output from the ANN
    private double accelerate, decelerate, turbo, leftSwitch, rightSwitch;

    double fitness;

    //index position of closest mine
    int closestOpponent;

    private CarId carId;
    private Race race;
    private CarPosition[] otherCars;
    private CarPosition carPosition;
    private TurboInfo turboInfo;
    private int gameTick;
    private boolean crashed;
    private boolean turboFired;
    private int lap;
    private LapFinished[] laps;

    private double distance;
    private double lastInPieceDistance;
    private boolean chrashed;

    public KotRaider() {
        throttle = 1.0;
    }

    public void incrementFitness() {
        ++fitness;
    }

    public double getFitness() {
        return fitness;
    }

    public void putWeights(List<Double> w) {
        neuralNetwork.putWeights(w);
    }

    public int getNumberOfWeights() {
        return neuralNetwork.getNumberOfWeights();
    }

    public void setCarId(CarId carId) {
        this.carId = carId;
    }

    public CarId getCarId() {
        return carId;
    }

    public void setRace(Race race) {
        this.race = race;
        laps = new LapFinished[race.raceSession.laps];
        prepare();
    }

    private void prepare() {

    }

    public Race getRace() {
        return race;
    }

    public void updateCarPositions(int gameTick, CarPosition[] carPositions) {
        this.gameTick = gameTick;
        otherCars = new CarPosition[carPositions.length - 1];
        int i = 0;
        for (CarPosition position : carPositions) {
            if (position.id.equals(this.carId)) {
                this.carPosition = position;
            } else {
                otherCars[i++] = position;
            }
        }

        distance += carPosition.piecePosition.inPieceDistance;
        lastInPieceDistance = carPosition.piecePosition.inPieceDistance;
        if (gameTick > 0)
            speed = distance / gameTick;
        //this will store all the inputs for the NN
        List<Double> inputs = new ArrayList<>();

        inputs.add((double) carPosition.piecePosition.pieceIndex);
        inputs.add(carPosition.piecePosition.inPieceDistance);
        inputs.add(carPosition.angle);
        inputs.add((double) carPosition.piecePosition.lap);
        inputs.add((double) carPosition.piecePosition.lane.startLaneIndex);
        inputs.add((double) carPosition.piecePosition.lane.endLaneIndex);

        //update the network and get feedback
        List<Double> outputs = neuralNetwork.update(inputs);

        if (outputs.size() < Constants.NUMBER_OUTPUTS) {
            System.out.println("Error in generating outputs");
        }
        //System.out.println("NN: "+ outputs);

        //assign the outputs to the raider's left & right tracks
        accelerate = outputs.get(0);
        decelerate = outputs.get(1);
        leftSwitch = outputs.get(2);
        rightSwitch = outputs.get(3);
        turbo = outputs.get(4);

    }

    public CarPosition getCarPosition() {
        return carPosition;
    }

    public void setTurboInfo(TurboInfo turboInfo) {
        this.turboInfo = turboInfo;
    }

    public TurboInfo getTurboInfo() {
        return turboInfo;
    }

    public void setCrashed(int gameTick) {
        crashed = true;
        this.gameTick = gameTick;
    }

    public void setRestored(int gameTick) {
        crashed = false;
        this.gameTick = gameTick;
    }

    public BotMsg getAction() {
        if (accelerate > 0) {
            this.throttle = Utils.clamp(this.throttle + 0.1, 0.5, 1.0);
            return new Throttle(this.gameTick, this.throttle);
        }
        if (decelerate > 0) {
            this.throttle = Utils.clamp(this.throttle - 0.1, 0.0, 1.0);
            return new Throttle(this.gameTick, this.throttle);
        }
        if (turbo > 0 && turboInfo != null) {
            return new Turbo(this.gameTick, "Catch me now!");
        }
        if (leftSwitch > 0) {
            return new SwitchLane(this.gameTick, "Left");
        }
        if (rightSwitch > 0) {
            return new SwitchLane(this.gameTick, "Right");
        }
        return new Ping(gameTick);
    }

    public boolean isTurboCharged() {
        return turboInfo != null;
    }

    public boolean isTurboFired() {
        return turboFired;
    }

    public void setTurboFired(boolean turboFired) {
        this.turboFired = turboFired;
    }

    public void startLap(int lap) {
        this.lap = lap;
    }

    public void finishLap(LapFinished lapDetails) {
        //this.laps[lap++] = lapDetails;
    }

    public boolean isChrashed() {
        return chrashed;
    }

    @Override
    public String toString() {
        return "{lap=" + lap +
                ", pieceIndex=" + (carPosition == null? 0: carPosition.piecePosition.pieceIndex) +
                ", throttle=" + throttle +
                '}';
    }

    public BotMsg getNextAction() {
        PiecePosition carPiecePosition = getCarPosition().piecePosition;
        TrackPiece previousPiece = getRace().track.pieces.get((int)Utils.clamp(carPiecePosition.pieceIndex - 1, 0, getRace().track.pieces.size()));
        TrackPiece currentPiece = getRace().track.pieces.get(carPiecePosition.pieceIndex);
        TrackPiece nextPiece = getRace().track.pieces.get((int)Utils.clamp(carPiecePosition.pieceIndex + 1, 0, getRace().track.pieces.size() - 1));

        if (isTurboCharged() && !isTurboFired() && !currentPiece.isBend()) {
            setTurboFired(true);
            return new Turbo(gameTick, "Catch me now!");
        }

        if (currentPiece != null){
            /*if(currentPiece.switchLane) {
                String direction = "Right";
                int fromLaneIndex = getCarPosition().piecePosition.lane.endLaneIndex;
                if (fromLaneIndex == getRace().track.lanes.size() - 1 || fromLaneIndex > 0) {
                    direction = "Left";
                }
                return new SwitchLane(gameTick, direction);
            } else*/ {
                if (nextPiece != null) {
                    if (nextPiece.isBend() || nextPiece.switchLane) {
                        if (currentPiece.isBend() || nextPiece.radius < currentPiece.radius) {
                            throttle = Utils.clamp(throttle - 0.1, 0.1, 1.0);
                        } else {
                            if(previousPiece.isBend()) {
                                throttle = Utils.clamp(throttle + 0.1, 0.1, 0.4);
                            }else{
                                throttle = Utils.clamp(throttle - 0.1, 0.4, 1.0);
                            }
                        }
                    } else {
                        throttle = Utils.clamp(throttle + 0.1, 0.1, 1.0);
                    }
                }
                return new Throttle(gameTick, throttle);
            }
        }else{
            return new Throttle(gameTick, Constants.MAX_THROTTLE);
        }
    }
}
