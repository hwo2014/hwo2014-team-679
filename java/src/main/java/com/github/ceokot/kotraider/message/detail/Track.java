package com.github.ceokot.kotraider.message.detail;

import java.util.List;

public class Track {
    public final String id;
    public final String name;
    public final List<TrackPiece> pieces;
    public final List<TrackLane> lanes;
    public final StartingPoint startingPoint;

    public Track(String id, String name, List<TrackPiece> pieces, List<TrackLane> lanes, StartingPoint startingPoint) {
        this.id = id;
        this.name = name;
        this.pieces = pieces;
        this.lanes = lanes;
        this.startingPoint = startingPoint;
    }
}
