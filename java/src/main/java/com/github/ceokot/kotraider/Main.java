package com.github.ceokot.kotraider;

import com.github.ceokot.kotraider.message.BotMsg;
import com.github.ceokot.kotraider.message.MsgWrapper;
import com.github.ceokot.kotraider.message.detail.CarId;
import com.github.ceokot.kotraider.message.detail.CarPosition;
import com.github.ceokot.kotraider.message.detail.TurboInfo;
import com.github.ceokot.kotraider.message.receive.GameInit;
import com.github.ceokot.kotraider.message.receive.LapFinished;
import com.github.ceokot.kotraider.message.send.Join;
import com.github.ceokot.kotraider.message.send.Ping;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class Main {
    private KotRaider raider;

    private Gson gson = new Gson();
    private PrintWriter writer;

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        BotMsg join = new Join(0, botName, botKey);
        //BotMsg joinMsg = new JoinRace(0, new BotId(botName, botKey), Constants.SUZUKA_TRACK, 1);
        new Main(reader, writer, join);
    }

    public Main(final BufferedReader reader, final PrintWriter writer, final BotMsg join) throws IOException {
        this.writer = writer;
        raider = new KotRaider();
        String line;
        send(join);

        while ((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                CarPosition[] carPositions = gson.fromJson(gson.toJson(msgFromServer.data), CarPosition[].class);
                raider.updateCarPositions(msgFromServer.gameTick, carPositions);
                send(raider.getNextAction());
            } else {
                switch (msgFromServer.msgType) {
                    case "yourCar":
                        CarId myCar = gson.fromJson(gson.toJson(msgFromServer.data), CarId.class);
                        raider.setCarId(myCar);
                        break;
                    case "gameInit":
                        GameInit game = gson.fromJson(gson.toJson(msgFromServer.data), GameInit.class);
                        raider.setRace(game.race);
                        break;
                    case "gameStart":
                        raider.startLap(0);
                        break;
                    case "lapFinished":
                        LapFinished lapDetails = gson.fromJson(gson.toJson(msgFromServer.data), LapFinished.class);
                        raider.finishLap(lapDetails);
                        break;
                    case "turboAvailable":
                        TurboInfo turboInfo = gson.fromJson(gson.toJson(msgFromServer.data), TurboInfo.class);
                        raider.setTurboInfo(turboInfo);
                        break;
                    case "crash":
                        raider.setCrashed(msgFromServer.gameTick);
                        break;
                    case "spawn":
                        raider.setRestored(msgFromServer.gameTick);
                        break;
                }

                System.out.print(msgFromServer.msgType + "@" + msgFromServer.gameTick + ": " + raider + ": ");
                System.out.println(msgFromServer.data);
                send(new Ping(msgFromServer.gameTick));

            }
        }
    }

    private void send(final BotMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

}
