package com.github.ceokot.kotraider.message.send;

import com.github.ceokot.kotraider.message.BotMsg;

public class Turbo extends BotMsg {
    private String turboMessage;

    public Turbo(int gameTick, String message) {
        super(gameTick);
        this.turboMessage = message;
    }

    @Override
    protected Object msgData() {
        return turboMessage;
    }
    @Override
    protected String msgType() {
        return "turbo";
    }

}
