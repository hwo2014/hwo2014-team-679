package com.github.ceokot.kotraider.message.detail;

import java.util.List;

public class Race {
    public final Track track;
    public  final List<Car> cars;
    public  final RaceSession raceSession;

    public Race(Track track, List<Car> cars, RaceSession raceSession) {
        this.track = track;
        this.cars = cars;
        this.raceSession = raceSession;
    }
}
