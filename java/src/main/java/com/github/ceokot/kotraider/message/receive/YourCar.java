package com.github.ceokot.kotraider.message.receive;

import com.github.ceokot.kotraider.message.BotMsg;

public class YourCar extends BotMsg {
    public final String name;
    public final String color;

    public YourCar(int gameTick, String name, String color) {
        super(gameTick);
        this.name = name;
        this.color = color;
    }

    @Override
    protected String msgType() {
        return "yourCar";
    }

}
