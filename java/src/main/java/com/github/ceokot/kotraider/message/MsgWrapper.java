package com.github.ceokot.kotraider.message;

public class MsgWrapper {
    public final int gameTick;
    public final String msgType;
    public final Object data;

    public MsgWrapper(int gameTick, final String msgType, final Object data) {
        this.gameTick = gameTick;
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(int gameTick, BotMsg botMsg) {
        this(gameTick, botMsg.msgType(), botMsg.msgData());
    }
}
