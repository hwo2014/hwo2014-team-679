package com.github.ceokot.kotraider.message.detail;

public class TrackLane {
    public final double distanceFromCenter;
    public final double index;

    public TrackLane(double distanceFromCenter, double index) {
        this.distanceFromCenter = distanceFromCenter;
        this.index = index;
    }
}
