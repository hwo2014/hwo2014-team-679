package com.github.ceokot.kotraider.message.send;

import com.github.ceokot.kotraider.message.BotMsg;
import com.github.ceokot.kotraider.message.detail.BotId;

public class JoinRace extends BotMsg {
    public final BotId botId;
    private final String trackName;
    public final int carCount;

    public JoinRace(int gameTick, BotId botId, String trackName, int carCount) {
        super(gameTick);
        this.botId = botId;
        this.trackName = trackName;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }

}
