package com.github.ceokot.kotraider.message.receive;

import com.github.ceokot.kotraider.message.detail.CarPosition;
import com.github.ceokot.kotraider.message.BotMsg;

import java.util.List;

public class CarPositions extends BotMsg {
    public final String gameId;
    public final int gameTick;
    public final List<CarPosition> carPositions;

    public CarPositions(String gameId, int gameTick, List<CarPosition> carPositions) {
        super(gameTick);
        this.gameId = gameId;
        this.gameTick = gameTick;
        this.carPositions = carPositions;
    }

    @Override
    protected String msgType() {
        return "carPositions";
    }

}
