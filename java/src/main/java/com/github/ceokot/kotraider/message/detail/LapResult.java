package com.github.ceokot.kotraider.message.detail;

public class LapResult {
    public final int laps;
    public final int ticks;
    public final int millis;

    public LapResult(int laps, int ticks, int millis) {
        this.laps = laps;
        this.ticks = ticks;
        this.millis = millis;
    }
}
